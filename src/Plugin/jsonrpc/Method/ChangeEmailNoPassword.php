<?php

declare(strict_types=1);

namespace Drupal\jsonrpc_change_email_no_password\Plugin\jsonrpc\Method;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\jsonrpc\Exception\JsonRpcException;
use Drupal\jsonrpc\JsonRpcObject\Error;
use Drupal\jsonrpc\JsonRpcObject\ParameterBag;
use Drupal\jsonrpc\MethodInterface;
use Drupal\jsonrpc\Plugin\JsonRpcMethodBase;
use Drupal\jsonrpc_change_email_no_password\Exception\JsonRpcChangeEmailNoPasswordException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Allows the user to change their email address without inputting a password.
 *
 * @JsonRpcMethod(
 *   id = "user.change_email_no_password",
 *   usage = @Translation("Change the user's email address, no password required."),
 *   access = {"jsonrpc change email no password"},
 *   params = {
 *      "mail" = @JsonRpcParameterDefinition(
 *        schema={"type"="string"},
 *        required=true
 *      ),
 *    }
 * ),
 */
final class ChangeEmailNoPassword extends JsonRpcMethodBase {

  /**
   * Logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * The logger channel to use for errors with this endpoint.
   */
  private const string JSONRPC_LOGGER_CHANNEL = 'exception_jsonrpc_email';

  /**
   * The parameter for the user's email address.
   */
  private const string PARAM_EMAIL = 'mail';

  public function __construct(
    array $configuration,
    string $plugin_id,
    MethodInterface $plugin_definition,
    protected AccountInterface $currentUser,
    protected LoggerChannelFactoryInterface $loggerFactory,
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->logger = $this->loggerFactory->get(self::JSONRPC_LOGGER_CHANNEL);
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): ChangeEmailNoPassword {
    if (!$plugin_definition instanceof MethodInterface) {
      throw new JsonRpcChangeEmailNoPasswordException('Plugin definition should be MethodInterface!');
    }
    return new self(
      $configuration, $plugin_id, $plugin_definition,
      $container->get('current_user'),
      $container->get('logger.factory'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function execute(ParameterBag $params): string {
    try {
      $email_address = $params->get(self::PARAM_EMAIL);
      if (!is_string($email_address)) {
        return 'Error: Invalid email address.';
      }
      $uid = $this->currentUser->id();
      $user_storage = $this->entityTypeManager->getStorage('user');
      $account = $user_storage->load($uid);
      /** @var \Drupal\user\UserInterface $account */
      if ($account->hasRole('admin')) {
        return 'Error: Admins cannot change their passwords here.';
      }
      $account->setEmail($email_address);
      if (boolval($account->save())) {
        return $email_address;
      }
      else {
        return 'Error: Failed to save account.';
      }
    }
    /* @phpstan-ignore-next-line Thrown exceptions in catch block must bundle previous exception. */
    catch (\LogicException $e) {
      $error_message = $e->getMessage();
      $this->logger->error($error_message);
      $error = Error::internalError();
      throw JsonRpcException::fromError($error);
    }
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public static function outputSchema(): array {
    return ['type' => 'string'];
  }

}
