<?php

declare(strict_types=1);

namespace Drupal\jsonrpc_change_email_no_password\Exception;

/**
 * Exceptions for the JSON-RPC Change Email No Password module.
 */
class JsonRpcChangeEmailNoPasswordException extends \Exception {
}
