<?php

namespace Drupal\Tests\jsonrpc_change_email_no_password\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test installation and uninstallation.
 *
 * @group jsonrpc_change_email_no_password
 */
class JsonRpcChangeEmailNoPasswordInstallTest extends KernelTestBase {

  private const string MODULE_NAME = 'jsonrpc_change_email_no_password';

  /**
   * Test that the module can be installed and uninstalled.
   */
  public function testInstallUninstall(): void {
    \Drupal::service('module_installer')->install([self::MODULE_NAME]);

    \Drupal::service('module_installer')->uninstall([self::MODULE_NAME]);

    // Try installing and uninstalling again.
    \Drupal::service('module_installer')->install([self::MODULE_NAME]);

    \Drupal::service('module_installer')->uninstall([self::MODULE_NAME]);
  }

}
