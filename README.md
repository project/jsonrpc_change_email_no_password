JSON-RPC Change Email No Password
==================================

This module provides a JSON-RPC endpoint that enables users to change their
email addresses without inputting their passwords.

It is similar to the
[No Current Password](https://www.drupal.org/project/nocurrent_pass) module but
for decoupled applications.

Only non-admin users can change their passwords! For security reasons, users
with an admin role are prevented from changing their passwords.

## Installation

1. Add the module with composer.
2. Enable the module.
3. Give the necessary users the permission
   `Change one's email address without a password via JSON-RPC`.
